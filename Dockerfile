FROM node:10-alpine AS front

WORKDIR /opt/front

ADD package.json .
ADD yarn.lock .

RUN yarn
COPY . .

CMD ["yarn", "start:dev"]
