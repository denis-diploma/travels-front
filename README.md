# travels-front

## Env variables
```dotenv
VUE_APP_GRAPHQL_HOST=http://{{api_host}}:{{api_port}}/graphql
```

## Docker
### Build image
```
docker build -t antsiferov/travels-front-image .
```
### Run container
```
docker run --rm --name travels-front -p 8090:8090 antsiferov/travels-front-image:latest
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Run your end-to-end tests
```
yarn run test:e2e
```

### Run your unit tests
```
yarn run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
