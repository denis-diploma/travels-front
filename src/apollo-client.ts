import {ApolloClient} from 'apollo-client'
import {HttpLink} from 'apollo-link-http'
import {WebSocketLink} from 'apollo-link-ws'
import { split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';
import { InMemoryCache } from 'apollo-cache-inmemory'

import {config} from '@/config';

const httpLink = new HttpLink({
    uri: config.graphqlHost,
});

const wsLink = new WebSocketLink({
    uri: config.graphqlWs,
    options: {
        reconnect: true,
    },
});

const link = split(
    ({ query }) => {
        const { kind, operation } = getMainDefinition(query) as any;

        return kind === 'OperationDefinition' &&
            operation === 'subscription'
    },
    wsLink,
    httpLink
);

const defaultOptions: any = {
    watchQuery: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'ignore',
    },
    query: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'all',
    },
};

export const apolloClient = new ApolloClient({
    link,
    cache: new InMemoryCache(),
    connectToDevTools: true,
    defaultOptions: defaultOptions,
});
