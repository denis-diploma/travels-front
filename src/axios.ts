import a from 'axios';
import {config} from '@/config';

export const axios = a.create({
    baseURL: config.apiHost
});
