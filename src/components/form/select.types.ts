export interface ISelectOption {
    title: string;
    value: string;
}
