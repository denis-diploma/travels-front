export const config: IConfig = {
    staticHost: process.env.VUE_APP_STATIC_HOST || '',
    apiHost: process.env.VUE_APP_API || '',
    graphqlHost: process.env.VUE_APP_GRAPHQL_HOST || '',
    graphqlWs: process.env.VUE_APP_GRAPHQL_WS || '',
    getStaticUrl(path: string) {
        return `${process.env.VUE_APP_STATIC_HOST}${path}`
    }
};

interface IConfig {
    [key: string]: string | Function;
    staticHost: string;
    apiHost: string;
    graphqlHost: string;
    graphqlWs: string;
    getStaticUrl: (path: string) => string;
}
