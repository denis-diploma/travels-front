import TextInput from './text-input.vue';
import CheckboxInput from './checkbox-input.vue';

export {
    TextInput,
    CheckboxInput
};