import {IPlace} from '@/components';

export class GApi {
    private gapi: IGApi;
    private markers: Marker[] = [];

    initialize(payload: IInitializePayload) {
        if ((window as any).google) {
            this.gapi = initializeGAPI(payload);
        } else {
            setTimeout(() => {
                this.gapi = initializeGAPI(payload);
            }, 100);
        }
    }

    setMarker = (id: string, place: IPlace, elementInfo?: HTMLElement) => {
        let marker;
        const index = this.markers
            .map(item => item.id)
            .indexOf(id);

        if (index > -1) {
            this.markers[index].marker.setPosition(place.location);
            marker = this.markers[index].marker;
        } else {
            marker = this.createMarker(place);
            this.markers.push({id, marker});

            if (elementInfo) {
                const info = new this.gapi.google.maps.InfoWindow();
                info.setContent(elementInfo);
                info.open(this.gapi.map, marker);
            }
        }

        return marker;
    };

    drawRoute = (origin: Location, destination: Location) => {
        const request = {origin, destination, travelMode: 'DRIVING'};

        this.gapi.directionsService.route(request, (result: any, status: string) => {
            if (status === 'OK') {
                this.gapi.directionsDisplay.setDirections(result);
            }
        });
    };

    formatPlace(googlePlace: any): IPlace {
        const name = [
            googlePlace.address_components[0] && googlePlace.address_components[0].long_name,
            googlePlace.address_components[2] && googlePlace.address_components[2].long_name,
            googlePlace.address_components[3] && googlePlace.address_components[3].long_name,
        ].join(', ');

        return {
            name,
            location: {
                lat: googlePlace.geometry.location.lat(),
                lng: googlePlace.geometry.location.lng(),
            }
        }
    }

    initAutocomplate = (input: HTMLInputElement, handlePlaceChange: Function) => {
        setTimeout(() => {
            const autocomplete = new this.gapi.google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', this.gapi.map);
            autocomplete.setFields(
                ['address_components', 'geometry', 'name']
            );

            autocomplete.addListener('place_changed', () => {
                const place = autocomplete.getPlace();
                handlePlaceChange(place);
            });
        }, 200)
    };

    private createMarker = (place: IPlace) => {
        return this.gapi && new this.gapi.google.maps.Marker({
            position: place.location,
            map: this.gapi.map,
            title: place.name,
        })
    };
}

export const initializeGAPI = ({element, center, zoom = 4.5, disableDefaultUI = false}: IInitializePayload) => {
    const google = (window as any).google;
    const googleCenter = new google.maps.LatLng(center.lat, center.lng);

    const map = new google.maps.Map(element, {
        center: googleCenter,
        zoom,
        disableDefaultUI
    });

    const directionsService = new google.maps.DirectionsService();
    const directionsDisplay = new google.maps.DirectionsRenderer();
    const service = new google.maps.places.PlacesService(map);
    directionsDisplay.setMap(map);

    return {google, map, directionsService, directionsDisplay, service}
};

interface IInitializePayload {
    element: HTMLElement;
    center: Location;
    zoom: number;
    disableDefaultUI?: boolean;
}

export interface Location {
    lat: number;
    lng: number;
}

export interface Marker {
    id: string;
    marker: any;
    info?: any;
}

interface IGApi {
    google: any;
    map: any;
    directionsService: any;
    directionsDisplay: any;
    service: any;
}
