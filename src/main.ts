import Vue from 'vue';
import VueApollo from 'vue-apollo';
import 'moment/locale/ru';

import {apolloClient} from './apollo-client';
import App from './App.vue';
import router from './router';
import {store} from './store';
import './register-service-worker';

Vue.use(VueApollo);
Vue.config.productionTip = false;
const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});

new Vue({
  apolloProvider,
  router,
  store,
  render: h => h(App)
}).$mount('#app');
