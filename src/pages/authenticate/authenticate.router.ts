import {LoginPage, RegistrationPage, AuthenticatePage} from './';

export const AuthenticateRouter = {
    path: 'authenticate',
    component: AuthenticatePage,
    children: [
        {
            path: 'login',
            name: 'Login',
            component: LoginPage,
        },
        {
            path: 'registration',
            name: 'Registration',
            component: RegistrationPage,
        },
        {
            path: '/authenticate',
            redirect: '/authenticate/login'
        },
    ]
};
