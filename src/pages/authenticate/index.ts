import LoginPage from './pages/login.page.vue';
import RegistrationPage from './pages/registration.page.vue';
import AuthenticatePage from './authenticate.page.vue';

export {
    LoginPage,
    RegistrationPage,
    AuthenticatePage
};
export {AuthenticateRouter} from './authenticate.router';
