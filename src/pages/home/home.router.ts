import {HomePage} from './';
import {CreateTripRouter} from './pages/create-trip';
import {ListTripsRouter} from './pages/list-trips';
import {UserTripsRouter} from './pages/user-trips';
import {ProfileRouter} from './pages/profile';
import {TripDetailsRouter} from './pages/trip-details';
import {ChatRouter} from './pages/chat';
import {UserDetailsRouter} from './pages/user-details';
import {SearchTripsRouter} from './pages/search-trips';

export const HomeRouter = {
    path: 'home',
    name: 'Home',
    component: HomePage,
    meta: {
        auth: true
    },
    children: [
        CreateTripRouter,
        ListTripsRouter,
        UserTripsRouter,
        ProfileRouter,
        TripDetailsRouter,
        UserDetailsRouter,
        ChatRouter,
        SearchTripsRouter,
        {
            path: '/home',
            redirect: `/home/${ListTripsRouter.path}`
        }
    ]
};
