import {ChatPage} from './';
import ChatsListPage from './pages/chats-list.page.vue';
import ChatMessagesPage from './pages/chat-messages.page.vue';
import DialogMessagesPage from './pages/dialog-messages.page.vue';

export const ChatRouter = {
    path: 'chat',
    component: ChatPage,
    children: [
        {
            path: 'list',
            component: ChatsListPage,
            meta: {
                burgerMenuBack: true
            }
        },
        {
            path: 'trip/:chatId',
            component: ChatMessagesPage,
        },
        {
            path: 'dialog/:dialogId',
            component: DialogMessagesPage,
        }
    ]
};
