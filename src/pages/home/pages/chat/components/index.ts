import MessageComponent from './message.component.vue';
import MessageInput from './message-input.component.vue';
import MessagesPage from './messages-page.component.vue';

export {MessageComponent, MessageInput, MessagesPage};
