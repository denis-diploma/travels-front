export const companionList = [
    {value: 'man', title: 'Попутчика'},
    {value: 'woman', title: 'Попутчицу'},
    {value: 'woman-company', title: 'Женскую компанию'},
    {value: 'man-company', title: 'Мужскую компанию'},
    {value: 'group', title: 'Собираю группу'},
];
