import {CreateTripPage} from './';

export const CreateTripRouter = {
    path: '/home/create-trip',
    component: CreateTripPage,
    name: 'Create Trip',
    meta: {
        burgerMenuBack: true,
        title: 'Создать поездку'
    }
};
