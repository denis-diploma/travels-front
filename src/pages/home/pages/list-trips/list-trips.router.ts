import {ListTripsPage} from './';

export const ListTripsRouter = {
    path: 'list',
    component: ListTripsPage,
    name: 'List trips',
    meta: {
        title: 'Все поездки'
    }
};
