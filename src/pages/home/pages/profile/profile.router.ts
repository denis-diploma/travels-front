import {ProfilePage} from './';
import {ProfileEditPage} from './pages/profile-edit';
import {UserProfile} from './pages/user-profile';

export const ProfileRouter = {
    path: 'profile',
    name: 'Profile',
    component: ProfilePage,
    meta: {
        burgerMenuBack: true,
        title: 'Профиль'
    },
    children: [
        {
            path: 'user',
            name: 'Profile',
            component: UserProfile,
        },
        {
            path: 'edit',
            name: 'ProfileEdit',
            component: ProfileEditPage
        }
    ],
};
