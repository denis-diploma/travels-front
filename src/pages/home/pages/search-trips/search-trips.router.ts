import {SearchTrips} from './';

export const SearchTripsRouter = {
    path: 'search-trips',
    component: SearchTrips,
};
