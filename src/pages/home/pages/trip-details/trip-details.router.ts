import {TripDetailsPage} from './';

export const TripDetailsRouter = {
    path: 'trip-details/:id',
    component: TripDetailsPage,
    meta: {
        burgerMenuBack: true,
        title: 'Детали поездки'
    }
};
