import {UserDetails} from './';

export const UserDetailsRouter = {
    path: 'user-details/:id',
    component: UserDetails,
    meta: {
        burgerMenuBack: true,
    }
};
