import {UserTripsPage} from './';

export const UserTripsRouter = {
    path: 'user-trips',
    name: 'User trips',
    component: UserTripsPage,
    meta: {
        burgerMenuBack: true,
        title: 'Мои поездки'
    }
};
