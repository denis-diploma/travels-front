import Vue from 'vue'
import Router from 'vue-router'
import RootPage from './pages/root.page.vue'
import {HomeRouter} from './pages/home'
import {AuthenticateRouter} from './pages/authenticate';
import {AUTH_KEY} from '@/store/modules/user/user.vuex';

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/',
            component: RootPage,
            name: 'RootPage',
            children: [
                HomeRouter,
                AuthenticateRouter,
                {
                    path: '/',
                    redirect: '/home'
                },
            ]
        },
    ]
});

router.beforeEach((to, from, next) => {
    const isPrivateRoute = to.matched.some(route => route.meta.auth);
    const isAuthRoute = to.matched.some(route => route.path === '/authenticate');
    const auth = localStorage.getItem(AUTH_KEY) || '{}';
    const isAccessTokenExist = !!JSON.parse(auth).accessToken;

    if (isAuthRoute && isAccessTokenExist) {
        next('/home')
    }

    if (!isPrivateRoute) {
        return next();
    }

    if (isAccessTokenExist) {
        return next();
    }

    next('/authenticate');
});

export default router;
