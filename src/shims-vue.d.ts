declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module "*.svg" {
  const content: any;
  export default content;
}

declare module '*.graphql' {
  import {DocumentNode} from 'graphql';

  const value: DocumentNode;
  export = value;
}

declare type HTMLElementEvent<T extends HTMLElement> = Event & {
  target: T;
}
