export interface IBottomBarState {
    open: boolean,
}

const state: IBottomBarState = {
    open: true,
};

const mutations = {
    setOpen(state: IBottomBarState, isOpen: boolean) {
        state.open = isOpen;
    }
};

const getters = {
    open(state: IBottomBarState) {
        return state.open;
    }
};

const actions = {
    show(context: any) {
        context.commit('setOpen', true);
    },
    hide(context: any) {
        context.commit('setOpen', false);
    }
};

export const bottomBar = {
    state, mutations, getters, actions, namespaced: true,
};
