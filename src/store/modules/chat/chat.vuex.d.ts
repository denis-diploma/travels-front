import {ITripDetails} from '@/store/modules/trips/trips.vuex.types';

export interface IChatState {
    chatsList: Array<IChatItem>;
    chat: IChat | null;
    messagesSubscription: any | null;
    chatsSubscription: any | null;
}

export interface IChat {
    id: number;
    users: Array<IChatUser>
    messages: Array<IMessage>
    trip: ITripDetails
}

export interface IChatUser {
    id: number;
    user: IAuthorUser
}

export interface IMessage {
    id: number;
    author: any;
    data: string;
    timestamp: Date;
    chat: IChatItem
}

export interface IMessageAuthor {
    id: number;
    user: IAuthorUser;
    chat?: {id: number};
}

export interface IAuthorUser {
    userId: number;
    name: string;
}

export interface IChatItem {
    id: number;
    users: Array<IChatMember>;
    trip: ITripDetails;
}

export interface IChatItemUser extends IAuthorUser{
    photo: string;
}

export interface IChatMember {
    user: IChatItemUser
}
