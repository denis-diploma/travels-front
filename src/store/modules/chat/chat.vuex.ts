import {ActionContext, MutationTree} from "vuex";

import {apolloClient} from "@/apollo-client";
import {IChatState, IChat, IChatItem, IMessage} from './chat.vuex.d';
import {IRootState, Module} from '@/store/store.d';
import GET_CHATS from './graphql/get-chats.graphql';
import GET_CHAT from './graphql/get-chat.graphql';
import GET_MESSAGES from './graphql/get-messages.graphql';
import SEND_MESSAGE from './graphql/send-message.graphql';
import SUBSCRIBE_ON_NEW_MESSAGES from './graphql/subscribe-on-new-message.graphql';
import SUBSCRIBE_ON_NEW_CHATS from './graphql/subscribe-on-new-chat.graphql';

const state: IChatState = {
    chatsList: [],
    chat: null,
    messagesSubscription: null,
    chatsSubscription: null,
};

const mutations: MutationTree<IChatState> = {
    setChatList(state: IChatState, chatsList: Array<IChatItem>) {
        state.chatsList = chatsList;
    },
    setChat(state: IChatState, chat: IChat) {
        state.chat = chat;
    },
    setSubscriptionMessages(state: IChatState, subscription: any) {
        state.messagesSubscription = subscription;
    },
    setSubscriptionChat(state: IChatState, chatsSubscription: any) {
        state.chatsSubscription = chatsSubscription;
    },
    addMessage(state: IChatState, message: IMessage) {
        if (
            state.chat &&
            message.author.userId &&
            state.chat.id === message.chat.id
        )
            state.chat.messages = [...state.chat.messages, message];
            state.chatsList = state.chatsList.map((chat: IChatItem) => {
                return chat;
            });
    },
    addChat(state: IChatState, chat: IChatItem) {
        state.chatsList = [...state.chatsList, chat];
    }
};

const getters = {
    chatsList(state: IChatState): Array<IChatItem> {
        return state.chatsList;
    },
    chatsIds(state: IChatState) {
        return state.chatsList.map(chat => chat.id);
    },
    chat(state: IChatState): IChat | null {
        return state.chat;
    },
    chatMessages(state: IChatState) {
        return state.chat ? state.chat.messages : [];
    }
};

const actions = {
    async fetchChats(context: ActionContext<IChatState, IRootState>, userId: number) {
        const res = await apolloClient.query({
            query: GET_CHATS,
            variables: {user: userId},
        });

        context.commit('setChatList', res.data.data)
    },
    async fetchChat(context: ActionContext<IChatState, IRootState>, chatId: number) {
        const resChat = await apolloClient.query({
            query: GET_CHAT,
            variables: {chatId}
        });
        const resMessages = await apolloClient.query({
            query: GET_MESSAGES,
            variables: {chatId}
        });
        console.log('fetch');

        context.commit('setChat', {
            ...resChat.data.data,
            ...resMessages.data
        });
    },
    async subscriptionOnNewMessages(context: ActionContext<IChatState, IRootState>, chatsIds: Array<number>) {
        if (context.state.messagesSubscription !== null) {
            context.state.messagesSubscription.unsubscribe();
        }

        const subscription = apolloClient
            .subscribe({
                query: SUBSCRIBE_ON_NEW_MESSAGES,
                variables: {chatsIds}
            })
            .subscribe(data => {
                context.commit('addMessage', data.data.message)
            });

        context.commit('setSubscriptionMessages', subscription)
    },
    async subscriptionOnNewChats(context: ActionContext<IChatState, IRootState>, userId: number) {
        const subscription = apolloClient
            .subscribe({
                query: SUBSCRIBE_ON_NEW_CHATS,
                variables: {userId}
            })
            .subscribe(data => {
                context.commit('addChat', data.data.chat)
            });

        context.commit('setSubscriptionChat', subscription)
    },
    async sendMessage(
        context: ActionContext<IChatState, IRootState>,
        variables: { author: number, chat: number, message: string }
    ) {
        await apolloClient.mutate({
            mutation: SEND_MESSAGE,
            variables
        })
    },
    clearData(context: ActionContext<IChatState, IRootState>) {
        context.commit('setSubscription', null);
        context.commit('setChatList', []);
        context.commit('setChat', null);
    }
};

export const chat: Module<IChatState, IRootState> = {
    state,
    mutations,
    getters,
    actions,
    namespaced: true,
};
