import {config} from '@/config';
import {apolloClient} from '@/apollo-client';
import GET_USER_DIALOGS from './graphql/get-user-dialogs.graphql';
import GET_DIALOG_MESSAGES from './graphql/get-dialog-messages.graphql';
import GET_DIALOG from './graphql/get-dialog.graphql';
import CREATE_DIALOG from './graphql/create-dialog.graphql';
import SEND_MESSAGE from './graphql/send-message.graphql';
import SUBSCRIBE_ON_NEW_DIALOGS_MESSAGES from './graphql/subscribe-on-new-dialog-messages.graphql';
import SUBSCRIBE_ON_NEW_DIALOGS from './graphql/subscribe-on-new-dialogs.graphql';

const state = {
    newDialogId: null,
    dialogsList: [],
    dialog: null,
    messagesSubscription: null,
    dialogsSubscription: null,
};

const prepareDialog = (dialog: any, userId: number) => {
    if (!dialog) {
        return;
    }

    let user;
    let companion;

    if (dialog.creator.userId === userId) {
        user = dialog.creator;
        companion = dialog.addressee
    } else {
        companion = dialog.creator;
        user = dialog.addressee
    }

    return {
        ...dialog,
        user: {...user, photo: config.getStaticUrl(user.photo)},
        companion: {...companion, photo: config.getStaticUrl(companion.photo)},
    }
};

const mutations = {
    setDialogsList(state: any, dialogsList: any[]) {
        state.dialogsList = dialogsList;
    },
    addMessage(state: any, message: IDialogMessage) {
        if (state.dialog && state.dialog.id === message.dialog.id) {
            state.dialog.messages = [
                ...state.dialog.messages,
                message,
            ];
        }
    },
    setNewDialogId(state: any, id: number | null) {
        state.newDialogId = id;
    },
    setDialog(state: any, dialog: any) {
        state.dialog = dialog;
    },
    addDialog(state: any, dialog: any) {
        state.dialogsList = [...state.dialogsList, dialog];
    },
    setSubscriptionDialog(state: any, dialogsSubscription: any) {
        state.dialogsSubscription = dialogsSubscription;
    },
    setSubscriptionsMessages(state: any, messagesSubscriptions: any) {
        state.messagesSubscription = messagesSubscriptions;
    }
};

const getters = {
    dialogsList(state: any) {
        return state.dialogsList;
    },
    dialog(state: any) {
        return state.dialog;
    },
    newDialogId(state: any) {
        return state.newDialogId;

    }
};

const actions = {
    async fetchDialogs(context: any, userId: number) {
        const res = await apolloClient.query({
            query: GET_USER_DIALOGS,
            variables: {id: userId},
        });

        const dialogs = res.data.data.map((item: any) => prepareDialog(item, userId));

        context.commit('setDialogsList', dialogs)
    },
    async fetchDialog(context: any, payload: IFetchDialogInput) {
        const messages = await apolloClient.query({
            query: GET_DIALOG_MESSAGES,
            variables: {dialog: payload.dialogId}
        });
        const dialog = await apolloClient.query({
            query: GET_DIALOG,
            variables: {id: payload.dialogId}
        });

        console.log(messages.data.data);

        context.commit('setDialog', {
            ...prepareDialog(dialog.data.data, payload.userId),
            messages: messages.data.data,
        })
    },
    async createDialog(context: any, payload: ICreateDialogInput) {
        const existDialog = context.getters.dialogsList
            .filter(
                (item: any) => (
                    item.creator.userId === payload.addressee || item.addressee.userId === payload.addressee
                ))[0];

        if (existDialog) {
            context.commit('setNewDialogId', existDialog.id);
        } else {
            const res = await apolloClient.mutate({
                mutation: CREATE_DIALOG,
                variables: payload,
            });

            context.commit('setNewDialogId', res.data.data.id);
        }
    },
    subscriptionOnNewDialogs(context: any, userId: number) {
        const subscription = apolloClient
            .subscribe({
                query: SUBSCRIBE_ON_NEW_DIALOGS,
                variables: {userId}
            })
            .subscribe(data => {
                context.commit('addDialog', prepareDialog(data.data.data, userId))
            });

        context.commit('setSubscriptionDialog', subscription)
    },
    subscriptionOnNewDialogMessages(context: any, userId: number) {
        const subscription = apolloClient
            .subscribe({
                query: SUBSCRIBE_ON_NEW_DIALOGS_MESSAGES,
                variables: {userId}
            })
            .subscribe((event: any) => {
                context.commit('addMessage', event.data.data)
            });

        context.commit('setSubscriptionsMessages', subscription)
    },
    async sendMessage(context: any, payload: ISendMessageInput) {
        await apolloClient.mutate({
            mutation: SEND_MESSAGE,
            variables: payload
        });
    }
};

export const dialogs = {
    state, mutations, getters, actions, namespaced: true,
};

interface ISendMessageInput {
    author: number;
    dialog: number;
    message: string;
}

interface IFetchDialogInput {
    userId: number;
    dialogId: number;
}

interface ICreateDialogInput {
    creator: number;
    addressee: number;
}

interface IDialogMessage {
    id: number;
    data: string;
    timestamp: string;
    author: {
        userId: number;
        name: string;
        surname: string;
    };
    dialog: {
        id: number;
    };
}
