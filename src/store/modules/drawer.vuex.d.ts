export interface IDrawerState {
    open: boolean,
    visible: boolean,
    timerId: number | null,
    duration: number,
}
