import {ActionContext} from "vuex";
import axios from 'axios';

import {IRootState} from '@/store/store.d';
import {apolloClient} from '@/apollo-client';
import {ITripState, ITripItem, ITripDetails, IParticipant} from './trips.vuex.types';
import GET_USER_TRIPS from './graphql/get-user-trips.graphql';
import CREATE_TRIP from './graphql/create-trip.graphql';
import GET_FILTERED_TRIPS from './graphql/get-filtered-trips.graphql';
import GET_TRIPS from './graphql/get-trips.graphql';
import DELETE_TRIP from './graphql/delete-trip.graphql';
import GET_FAVORITE_TRIPS from './graphql/get-favorite-trips.graphql';
import ADD_FAVORITE_TRIP from './graphql/add-favorite-trip.graphql';
import REMOVE_FAVORITE_TRIP from './graphql/remove-favorite-trip.graphql';
import GET_TRIP_DETAILS from './graphql/get-trip-details.graphql';
import ADD_PARTICIPANT from './graphql/add-participant.graphql';
import REMOVE_PARTICIPANT from './graphql/remove-participant.graphql';
import SELECT_PARTICIPANT from './graphql/select-participant.graphql';
import UNSELECT_PARTICIPANT from './graphql/unselect-participant.graphql';

export enum ParticipantsStatuses {
    WAITING = 'WAITING',
    SELECTED = 'SELECTED',
}

const state: ITripState = {
    all: [],
    userTrips: [],
    tripDetails: null,
    filter: null,
    favorite: [],
};

const mutations = {
    setTrips(state: ITripState, trips: ITripItem[]) {
        state.all = trips;
    },
    addTrip(state: ITripState, trip: ITripItem) {
        state.all = [...state.all, trip];
    },
    setFavoriteTrips(state: ITripState, trips: ITripItem[]) {
        state.favorite = trips;
    },
    deleteTrip(state: ITripState, tripId: number) {
        state.all = state.all.filter(item => item.tripId !== tripId);
    },
    setFilter(state: ITripState, filter: any | null) {
        if (!filter) {
            return state.filter = filter;
        }

        state.filter = Object.keys(filter).reduce((acc: any, key: string) => {
            if (filter[key]) {
                return {[key]: filter[key], ...acc};
            } else {
                return acc;
            }
        }, {});
    },
    setUserTrips(state: ITripState, trips: ITripItem[]) {
        state.userTrips = trips;
    },
    setTripDetails(state: ITripState, trip: ITripDetails) {
      state.tripDetails = trip;
    },
    setParticipantStatus(state: ITripState, {id, status}: {id: number, status: ParticipantsStatuses}) {
        if (state.tripDetails) {
            state.tripDetails.participants = state.tripDetails.participants
                .map(
                    item => item.id === id ? {...item, status}: item
                );
        }
    },
    addParticipant(state: ITripState, participant: IParticipant) {
        if (state.tripDetails) {
            state.tripDetails.participants = [
                ...state.tripDetails.participants,
                participant,
            ];
        }
    },
    removeParticipant(state: ITripState, userId: number) {
        if (state.tripDetails) {
            state.tripDetails.participants = state.tripDetails.participants
                .filter((item: any) => item.user.userId !== userId);
        }
    }
};

const getters = {
    filter: (state: ITripState) => state.filter,
    trips: (state: ITripState) => state.all,
    userTrips: (state: ITripState) => state.userTrips,
    favorite: (state: ITripState) => state.favorite,
    tripDetails: (state: ITripState) => state.tripDetails
};

const actions = {
    async fetchTrips(context: ActionContext<ITripState, IRootState>, filter?: any) {
        let res;

        if (filter) {
            console.log(filter);

            res = await apolloClient.query({
                query: GET_FILTERED_TRIPS,
                variables: {filter}
            });
        } else {
            res = await apolloClient.query({
                query: GET_TRIPS,
            });
        }

        context.commit('setTrips', res.data.data)
    },
    async fetchFilteredTrips(context: ActionContext<ITripState, IRootState>, filter: any) {
        const res = await apolloClient.query({
            query: GET_FILTERED_TRIPS,
            variables: {filter}
        });

        context.commit('setTrips', res.data.data);
    },
    async createTrip(context: ActionContext<ITripState, IRootState>, trip: any) {
        const unsplashResponse = await axios.get(
            'https://api.unsplash.com/photos/random/?client_id=88a046b0a1a151e18749c28469574b78ca8cdb5991e7bac0954c83cec7124427&query=city'
        );
        const photo = unsplashResponse.data.urls.regular;


        const res = await apolloClient.mutate({
            mutation: CREATE_TRIP,
            variables: {...trip, photo}
        });

        context.commit('addTrip', res.data.createTrip);
    },
    async fetchUserTrips(context: ActionContext<ITripState, IRootState>, id: number) {
        const res = await apolloClient.query({
            query: GET_USER_TRIPS,
            variables: {
                id
            }
        });

        context.commit('setUserTrips', res.data.data);
    },
    async fetchTripDetails(context: ActionContext<ITripState, IRootState>, tripId: number) {
        context.commit('setTripDetails', {});

        const trip = await apolloClient.query({
            query: GET_TRIP_DETAILS,
            variables: {tripId}
        });

        context.commit('setTripDetails', trip.data.data);
    },
    async removeParticipant(context: ActionContext<ITripState, IRootState>, variables: {userId: number; tripId: number}) {
        await apolloClient.mutate({
            mutation: REMOVE_PARTICIPANT,
            variables
        });

        context.commit('removeParticipant', variables.userId)
    },
    async fetchFavoriteTrips(context: ActionContext<ITripState, IRootState>, userId: number) {
        const res = await apolloClient.query({
            query: GET_FAVORITE_TRIPS,
            variables: {userId},
        });

        context.commit('setFavoriteTrips', res.data.data);
    },
    async addFavorite(context: ActionContext<ITripState, IRootState>, variables: {userId: number, tripId: number}) {
        const res = await apolloClient.mutate({
            mutation: ADD_FAVORITE_TRIP,
            variables,
        });

        context.commit('setFavoriteTrips', res.data.data);
    },
    async removeFavorite(context: ActionContext<ITripState, IRootState>, variables: {userId: number, tripId: number}) {
        const res = await apolloClient.mutate({
            mutation: REMOVE_FAVORITE_TRIP,
            variables,
        });

        context.commit('setFavoriteTrips', res.data.data);
    },
    async deleteTrip(context: ActionContext<ITripState, IRootState>, tripId: number) {
        await apolloClient.mutate({
            mutation: DELETE_TRIP,
            variables: {tripId}
        });

        context.commit('deleteTrip', tripId);
    },
    async addParticipant(context: ActionContext<ITripState, IRootState>, data: any) {
        const res = await apolloClient.mutate({
            mutation: ADD_PARTICIPANT,
            variables: data,
        });

        context.commit('addParticipant', res.data.data)
    },
    async selectParticipant(context: ActionContext<ITripState, IRootState>, id: number) {
        await apolloClient.mutate({
            mutation: SELECT_PARTICIPANT,
            variables: {id},
        });

        context.commit('setParticipantStatus', {id, status: ParticipantsStatuses.SELECTED})
    },
    async unselectParticipant(context: ActionContext<ITripState, IRootState>, id: number) {
        await apolloClient.mutate({
            mutation: UNSELECT_PARTICIPANT,
            variables: {id},
        });

        context.commit('setParticipantStatus', {id, status: ParticipantsStatuses.WAITING})
    }
};

export const trips = {state, mutations, actions, getters, namespaced: true};
