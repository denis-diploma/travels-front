import {IPlace} from '@/components/form/place-input.types';

export interface ITripState {
    all: ITripItem[],
    userTrips: ITripItem[],
    tripDetails: ITripDetails | null,
    filter: any | null,
    favorite: ITripItem[],
}

export interface ICreateTripInput {
    local: boolean;
    from: IPlace;
    to: IPlace;
    dateStart: string;
    dateEnd: string;
    description: string;
    companion: string;
    withChildren: boolean;
    wishes: string;
    author?: number;
}

export interface ITrip {
    tripId: number;
    description: string;
    dateStart: string;
    dateEnd: string;
    photo: string;
}

export interface ITripItem extends ITrip{
    author: {name: string; surname: string;};
    from: {name: string;};
    to: {name: string};
    participants: {user: {photo: string}}[];
}

export interface ITripDetails extends ITrip {
    from: ITripPlace;
    to: ITripPlace;
    author: ITripUser;
    participants: IParticipant[];
}

interface ITripPlace {
    tripPlaceId: number;
    name: string;
    latitude: number;
    longitude: number;
}

export interface ITripUser {
    userId: number;
    photo: string;
    name: string;
    surname: string;
}

export interface IParticipant {
    id: number;
    status: string;
    user: ITripUser
}
