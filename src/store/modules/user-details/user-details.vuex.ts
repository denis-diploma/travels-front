import {apolloClient} from "@/apollo-client";
import GET_USER_TRIPS from '../trips/graphql/get-user-trips.graphql';
import GET_USER from '../user/graphql/get-user.graphql';

const state = {
    user: {},
    trips: [],
};

const mutations = {
    setDetails(state: any, details: any) {
        state.user = details.user;
        state.trips = details.trips;
    },
};

const getters = {
    getDetails(state: any) {
        return state;
    }
};

const actions = {
    async fetchUserDetails(context: any, userId: number) {
        const res = await Promise.all([
            apolloClient.query({
                query: GET_USER,
                variables: {id: userId}
            }),
            apolloClient.query({
                query: GET_USER_TRIPS,
                variables: {id: userId},
            }),
        ]);

        context.commit('setDetails', {
            user: res[0].data.data,
            trips: res[1].data.data,
        });
    }
};

export const userDetails = {
    state, mutations, getters, actions, namespaced: true
};
