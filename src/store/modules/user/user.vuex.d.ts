export interface IUserRegistrationInput {
    name: string;
    surname: string;
    email: string;
    password: string;
}

export interface IUserLoginInput {
    email: string;
    password: string;
}

export interface IUserStore {
    about: string | null;
    birthday: string | null;
    city: string | null;
    country: string | null;
    gender: string | null;
    photo: string | null;
    userId: number | null;
    email: string | null;
    phone: string | null;
    name: string | null;
    surname: string | null;
    accessToken: string | null;
    refreshToken: string | null;
}
