import {ActionContext, MutationTree} from "vuex";
import {axios} from '@/axios';
import moment from 'moment';

import {apolloClient} from "@/apollo-client";
import {IUserRegistrationInput, IUserStore, IUserLoginInput} from './user.vuex.d';
import {IRootState, Module} from '@/store/store.d';
import UPDATE_USER_INFO from './graphql/update-user-info.graphql';
import GET_USER from './graphql/get-user.graphql';
import avatar from '@/assets/images/man.svg';

const defaultState: IUserStore = {
    userId: null,
    about: null,
    email: null,
    phone: null,
    name: null,
    surname: null,
    birthday: null,
    city: null,
    country: null,
    gender: null,
    photo: null,
    accessToken: null,
    refreshToken: null,
};

export const AUTH_KEY = 'AUTH_KEY';
const state: IUserStore = {
    ...defaultState
};

const mutations: MutationTree<IUserStore> = {
    changePhoto(state: any, photo: string) {
        state.photo = photo;
    },
    setUser(state: any, user: any) {
        Object.keys(user).map(key => {
            state[key] = user[key];
        })
    }
};

const getters = {
    user(state: IUserStore) {
        return state;
    },
    userId(state: IUserStore): number | null {
        return state.userId
    },
    fullName(state: IUserStore): string | null {
        const fullName = `${state.name} ${state.surname}`;

        return fullName.trim() === '' ? null : fullName;
    },
    photo(state: IUserStore) {
        return state.photo
    },
    avatar(): string {
        return avatar;
    },
    email(state: IUserStore) {
        return state.email;
    },
    place(state: IUserStore): string | null {
        if (!state.country && !state.city) {
            return null;
        }

        if (!state.city) {
            return state.country;
        }

        if (!state.country) {
            return state.city;
        }

        return `${state.country}, ${state.city}`;
    },
    age(state: IUserStore): number | null {
        if (!state.birthday) {
            return null;
        }

        return moment().diff(state.birthday, 'years');
    }
};

const actions = {
    async registration(context: ActionContext<IUserStore, IRootState>, userInfo: IUserRegistrationInput) {
        try {
            const res = await axios.post('/authenticate/registration', userInfo);

            return res.data;
        } catch (e) {
            return e.response.data;
        }
    },
    async changePhoto(context: ActionContext<IUserStore, IRootState>, {id, photo}: {id: number, photo: any}) {
        const formData = new FormData();
        formData.append("file", photo);

        const res = await axios.post(`/authenticate/photo?id=${id}`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });

        context.commit('changePhoto', res.data.image);
    },
    async login(context: ActionContext<IUserStore, IRootState>, userData: IUserLoginInput) {
        try {
            const res = await axios.post('/authenticate/login', userData);

            const {accessToken, refreshToken, userId} = res.data.data;
            localStorage.setItem(AUTH_KEY, JSON.stringify({
                accessToken, refreshToken, userId
            }));

            context.commit('setUser', res.data.data);

            return {error: false}
        } catch (e) {
            return e.response.data;
        }
    },
    async updateUserInfo(context: ActionContext<IUserStore, IRootState>, user: any) {
        const res = await apolloClient.mutate({
            mutation: UPDATE_USER_INFO,
            variables: {
                user,
                id: context.state.userId
            }
        });

        context.commit('setUser', res.data.data)
    },
    async fetchUser(context: ActionContext<IUserStore, IRootState>) {
        const data = JSON.parse(
            localStorage.getItem(AUTH_KEY) || ''
        );

        const res = await apolloClient.query({
            query: GET_USER,
            variables: {
                id: data.userId
            }
        });

        context.commit('setUser', res.data.data);
    },
    exit(context: ActionContext<IUserStore, IRootState>) {
        localStorage.setItem(AUTH_KEY, '');
        context.commit('setUser', defaultState)
    }
};

export const user: Module<IUserStore, IRootState> = {
    state, mutations, actions, getters, namespaced: true
};

