import Vue from 'vue';
import Vuex, {StoreOptions} from 'vuex';

import {IRootState} from './store.d';
import {trips} from '@/store/modules/trips';
import {user} from '@/store/modules/user';
import {chat} from '@/store/modules/chat';
import {dialogs} from '@/store/modules/dialogs';
import {userDetails} from '@/store/modules/user-details';
import {bottomBar} from './modules/bottom-bar.vuex';

Vue.use(Vuex);

const pureStore: StoreOptions<IRootState> = {
  modules: {
    trips,
    user,
    chat,
    dialogs,
    userDetails,
    bottomBar,
  }
};

export const store = new Vuex.Store<IRootState>(pureStore);
