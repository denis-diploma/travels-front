module.exports = {
    devServer: {
        port: process.env.PORT || 3001
    },
    chainWebpack: (config) => {
        // GraphQL Loader
        config.module
            .rule('graphql')
            .test(/\.(graphql|gql)$/)
            .use('graphql-tag/loader')
            .loader('graphql-tag/loader')
            .end();
    },
};
